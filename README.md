# BracySocket
[![Build Status](https://scrutinizer-ci.com/b/stolnikov/bracy-socket/badges/build.png?b=code-review)](https://scrutinizer-ci.com/b/stolnikov/bracy-socket/build-status/code-review)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/stolnikov/bracy-socket/badges/quality-score.png?b=code-review)](https://scrutinizer-ci.com/b/stolnikov/bracy-socket/?branch=code-review)

Multi user TCP socket server on top of [stolnikov/bracy package](https://github.com/stolnikov/bracy).

# Requirements
*  PHP >= 7.1

# Server-side start options
**Homework 2:** `server:start <port>` command (`BracySocket\Commands\ServerCommand`).

**Homework 3:** `server:start-yaml <filepath>` command (`BracySocket\Commands\ServerYamlCommand`).

# Project structure
```
├── bin
│   └── console.php
├── composer.json
├── config.yml
├── docker-compose.yml
├── Dockerfile
├── README.md
└── src
    ├── Commands
    │   ├── AbstractCommand.php
    │   ├── ServerCommand.php
    │   └── ServerYamlCommand.php
    └── Server
        ├── Exceptions
        │   └── SocketException.php
        ├── Handlers
        │   ├── BracyDispatcher.php
        │   ├── DispatcherInterface.php
        │   └── HandlerInterface.php
        ├── Helpers
        │   └── YamlPortFetcher.php
        ├── Sockets
        │   ├── ClientSocketInterface.php
        │   ├── ClientSocket.php
        │   ├── MainSocketBuilderInterface.php
        │   ├── MainSocketBuilder.php
        │   ├── MainSocketInterface.php
        │   ├── MainSocket.php
        │   ├── TCPSocketInterface.php
        │   └── TCPSocket.php
        ├── SocketServerInterface.php
        ├── SocketServer.php
        ├── Validators
        │   ├── SocketValidatorInterface.php
        │   └── SocketValidator.php
        └── Workers
            ├── SocketWorkerInterface.php
            └── SocketWorker.php
```

# Server launch
`docker-compose up` builds a docker container socket server with CLI-script master-process:
```
ENTRYPOINT ["./bin/console.php", "server:start-yaml", "./config.yml"] 
```
Base image contains a minimal set of php extensions determined by `phpcompatinfo` package.

For security reasons a limited range of ports is mapped to local loopback device (127.0.0.1): namely, from `PORT_MIN=10000` to `PORT_MAX=10010`. Both values can be found in the project .env file.

Listening socket port is defined in config.yml and has a default value of 10000.

# Server socket restart on port change

Signal handing is done asynchronously by calling `pcntl_async_signals(true);` (as of PHP 7.1).
        
```
docker kill --signal="SIGHUP" bracy
```
Server-side output for valid port:
```
bracy    | Successfully started TCP server at port 10000.
bracy    | SIGHUP signal is captured.
bracy    | Configuration change detected. Creating a new socket on port 10005.
bracy    | Successfully started TCP server at port 10005.
```
Server-side output for invalid port:
```
bracy    | Successfully started TCP server at port 10000.
bracy    | SIGHUP signal is captured.
bracy    | Please specify valid integer value for the port.
bracy exited with code 0
```
# Usage
```
telnet localhost 10000
```
```
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.

Bracy TCP/IP Server.
You are the client No.: 0
'help' displays instruction on app usage.
```
```
help
```
```
Example query string: '()()((()))'{enter}
```

**Example 1.** Default parameters provided the following file contents:
```
((((()))()(())()))
```
Output:
```
Check complete. Brackets are balanced.
```
**Example 2.**
```
((*))[]
```
Output:
```
Provided string contains invalid characters.
```
**Example 3.** Empty string.

Output:
```
Provided string is empty.
```
