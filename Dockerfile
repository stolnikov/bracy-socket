FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
   php7.2-cli php-apcu php7.2-json \
   php7.2-mbstring php7.2-xml

WORKDIR /var/bracy

ENTRYPOINT ["./bin/console.php", "server:start-yaml", "./config.yml"]
