#!/usr/bin/env php
<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Bracy\Validators\BalancedValidator;
use Bracy\Validators\CharsValidator;
use BracySocket\Commands\ServerCommand;
use BracySocket\Commands\ServerYamlCommand;
use BracySocket\Server\Handlers\BracyDispatcher;
use Symfony\Component\Console\Application;
use Symfony\Component\Dotenv\Exception\PathException;

try {
    $app = new Application('Bracy TCP/IP server app', '1.0');

    $bracyDispatcher = new BracyDispatcher(
        new BalancedValidator(
            new CharsValidator()
        )
    );

    $app->add(new ServerYamlCommand($bracyDispatcher));
    $app->add(new ServerCommand($bracyDispatcher));

    $app->run();
} catch (PathException | \BadMethodCallException $e) {
    echo sprintf("%s", $e->getMessage());
} catch (\Throwable $e) {
    echo sprintf("The app failed to run." . PHP_EOL . "%s", $e->getMessage());
}
