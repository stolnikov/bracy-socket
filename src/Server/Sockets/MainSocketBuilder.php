<?php

namespace BracySocket\Server\Sockets;

use BracySocket\Server\Exceptions\SocketException;
use BracySocket\Server\Validators\SocketValidator;

/**
 * Implementation of main socket builder.
 */
class MainSocketBuilder implements MainSocketBuilderInterface
{
    /**
     * Listening socket resource descriptor
     *
     * @var resource
     */
    private $descriptor;

    /**
     * IP address in dotted-quad notation (i.e. 127.0.0.1)
     *
     * @var string
     */
    private $address = '0.0.0.0';

    /**
     * The port on which to listen for connections
     *
     * @var int
     */
    private $port = 10000;

    /**
     * Maximum number of incoming connections queued for processing
     * Historically, this has been restricted to a maximum of 5.
     *
     * @var int
     */
    private $backlog = 5;

    /**
     * Reports whether local addresses can be reused.
     *
     * @var bool
     */
    private $reuseAddress = false;

    /**
     * {@inheritdoc}
     */
    public function create(): MainSocketBuilder
    {
        $this->descriptor = socket_create(
            AF_INET,
            SOCK_STREAM,
            SOL_TCP
        );

        if (!$this->descriptor) {
            throw new SocketException();
        }

        socket_set_option(
            $this->descriptor,
            SOL_SOCKET,
            SO_REUSEADDR,
            $this->reuseAddress
        );

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function bind(): MainSocketBuilder
    {
        if (!socket_bind(
            $this->descriptor,
            $this->address,
            $this->port
        )
        ) {
            throw new SocketException();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function listen(): MainSocketBuilder
    {
        if (!socket_listen(
            $this->descriptor,
            $this->backlog
        )
        ) {
            throw new SocketException();
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build(): MainSocket
    {
        $socket = new MainSocket($this->descriptor);

        // socket state validation procedure
        if (!(new SocketValidator($socket))->isValid()) {
            throw new SocketException();
        }

        return new MainSocket($this->descriptor);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddress(string $address): MainSocketBuilder
    {
        $this->address = $address;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPort(int $port): MainSocketBuilder
    {
        $this->port = $port;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setBacklog(int $backlog): MainSocketBuilder
    {
        $this->backlog = $backlog;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setReuseAddress(bool $reuseAddress): MainSocketBuilder
    {
        $this->reuseAddress = $reuseAddress;

        return $this;
    }
}
