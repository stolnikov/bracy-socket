<?php

namespace BracySocket\Server\Sockets;

use BracySocket\Server\Exceptions\SocketException;

/**
 *  Client socket implementation.
 */
class ClientSocket extends TCPSocket implements ClientSocketInterface
{
    /**
     * {@inheritdoc}
     */
    public function read(int $bufferSize = 2048, int $readMode = PHP_BINARY_READ)
    {
        $buffer = @socket_read($this->descriptor, $bufferSize, $readMode);

        if ($buffer === false) {
            throw new SocketException();
        }

        return $buffer;
    }

    /**
     * {@inheritdoc}
     */
    public function write(
        $responseBody
    ) {
        $bytesSent = @socket_write(
            $this->descriptor,
            $responseBody,
            strlen($responseBody)
        );

        if ($bytesSent === false) {
            throw new SocketException();
        }
    }
}
