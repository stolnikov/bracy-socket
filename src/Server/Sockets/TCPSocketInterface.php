<?php

namespace BracySocket\Server\Sockets;

/**
 * Abstraction of a basic TCP socket.
 */
interface TCPSocketInterface
{
    /**
     * Return socket resource handle.
     *
     * @return resource
     */
    public function getDescriptor();

    /**
     * Close the socket
     *
     * @return void
     */
    public function close();
}
