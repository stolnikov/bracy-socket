<?php

namespace BracySocket\Server\Sockets;

use BracySocket\Server\Exceptions\SocketException;

/**
 *  Client socket abstraction.
 */
interface ClientSocketInterface extends TCPSocketInterface
{
    /**
     * Read from the socket.
     *
     * @param int $bufferSize
     * @param int $readMode
     * PHP_NORMAL_READ = 1 or PHP_BINARY_READ = 2
     *
     * @return string the information read from the socket
     *
     * @throws SocketException
     */
    public function read(int $bufferSize = 2048, int $readMode = PHP_BINARY_READ);

    /**
     * Write to the socket.
     *
     * @param string $responseBody
     */
    public function write($responseBody);
}
