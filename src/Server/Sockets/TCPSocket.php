<?php

namespace BracySocket\Server\Sockets;

/**
 * Basic TCP socket class.
 */
class TCPSocket implements TCPSocketInterface
{
    /**
     * Socket descriptor
     *
     * @var resource
     */
    protected $descriptor;

    /**
     * TCPSocket constructor
     *
     * @param $descriptor
     */
    public function __construct($descriptor)
    {
        $this->descriptor = $descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescriptor()
    {
        return $this->descriptor;
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        socket_close($this->descriptor);
    }
}
