<?php

namespace BracySocket\Server\Sockets;

/**
 * Listening socket abstraction.
 * Its only concern is to create new client sockets on connection request
 * from a client. It never performs read or write operations.
 */
interface MainSocketInterface extends TCPSocketInterface
{
    /**
     * Create a socket for a connected client.
     *
     * @return ClientSocketInterface
     */
    public function accept(): ClientSocketInterface;
}
