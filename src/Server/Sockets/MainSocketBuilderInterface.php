<?php

namespace BracySocket\Server\Sockets;

use BracySocket\Server\Exceptions\SocketException;

/**
 * MainSocketBuilder abstraction.
 */
interface MainSocketBuilderInterface
{
    /**
     * Create a TCP socket.
     *
     * @return MainSocketBuilder
     *
     * @throws SocketException
     */
    public function create(): MainSocketBuilder;

    /**
     * Bind the socket to address and port.
     *
     * @return MainSocketBuilder
     *
     * @throws SocketException
     */
    public function bind(): MainSocketBuilder;

    /**
     * Start listening for connection requests.
     *
     * @return MainSocketBuilder
     *
     * @throws SocketException
     */
    public function listen(): MainSocketBuilder;

    /**
     * Build a listening socket object.
     *
     * @return MainSocket
     *
     * @throws SocketException
     */
    public function build(): MainSocket;

    /**
     * Set interface address for a main socket.
     *
     * @param string $address
     *
     * @return MainSocketBuilder
     */
    public function setAddress(string $address): MainSocketBuilder;

    /**
     * Set port number to listen on.
     *
     * @param int $port
     *
     * @return MainSocketBuilder
     */
    public function setPort(int $port): MainSocketBuilder;

    /**
     * Set backlog parameter.
     *
     * @param int $backlog
     *
     * @return MainSocketBuilder
     */
    public function setBacklog(int $backlog): MainSocketBuilder;

    /**
     * Set whether local addresses can be reused.
     *
     * @param bool $reuseAddress
     *
     * @return MainSocketBuilder
     */
    public function setReuseAddress(bool $reuseAddress): MainSocketBuilder;
}
