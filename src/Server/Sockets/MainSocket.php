<?php

namespace BracySocket\Server\Sockets;

use BracySocket\Server\Exceptions\SocketException;

/**
 * Listening socket implementation.
 */
class MainSocket extends TCPSocket implements MainSocketInterface
{
    /**
     * {@inheritdoc}
     */
    public function accept(): ClientSocketInterface
    {
        if (($clientDescriptor = socket_accept($this->descriptor)) === false) {
            throw new SocketException();
        }

        return (new ClientSocket($clientDescriptor));
    }
}
