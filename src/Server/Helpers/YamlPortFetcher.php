<?php

namespace BracySocket\Server\Helpers;

use Bracy\Exceptions\EmptyContentException;
use Symfony\Component\Yaml\Yaml;

class YamlPortFetcher
{
    /**
     * Parse .yml configuration file and return application port.
     *
     * @param string $filePath
     *
     * @return int
     *
     * @throws EmptyContentException
     * @throws \InvalidArgumentException
     */
    public static function getPort(string $filePath): int
    {
        if (is_readable($filePath) === false) {
            throw new EmptyContentException(
                sprintf(
                    "The file %s was not found.",
                    $filePath
                )
            );
        }

        $fileContents = Yaml::parseFile($filePath);
        $port = $fileContents['port'];

        if (self::isPortValid($port) === false) {
            throw new \InvalidArgumentException(
                sprintf("Please specify valid integer value for the port.")
            );
        }

        return $port;
    }

    /**
     * Check if a port is in the defined range.
     *
     * @param $port
     *
     * @return bool
     */
    private static function isPortValid($port): bool
    {
        $min = getenv('PORT_MIN');
        $max = getenv('PORT_MAX');
        $isInRange = filter_var(
            $port,
            FILTER_VALIDATE_INT,
            [
                'options' => [
                    'min_range' => $min,
                    'max_range' => $max,
                ]
            ]
        );

        return $isInRange;
    }
}
