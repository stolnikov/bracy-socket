<?php

namespace BracySocket\Server;

use BracySocket\Server\Handlers\HandlerInterface;
use BracySocket\Server\Sockets\MainSocketBuilder;
use BracySocket\Server\Workers\SocketWorker;
use BracySocket\Server\Workers\SocketWorkerInterface;

/**
 * Socket server implementation.
 */
class SocketServer implements SocketServerInterface
{
    /**
     * @var string
     */
    private $address;

    /**
     * Port to listen on
     *
     * @var int
     */
    private $port;

    /**
     * Output handler
     *
     * @var HandlerInterface
     */
    private $handler;

    /**
     * Server state switcher
     * Used to control working state of a worker.
     *
     * @var bool
     */
    private $serverState = false;

    /**
     * Socket worker
     *
     * @var SocketWorkerInterface
     */
    private $worker;

    /**
     * SocketServer constructor
     *
     * @param string $address
     * @param mixed $port
     * @param $handler
     */
    public function __construct(
        string $address,
        int $port,
        HandlerInterface $handler
    ) {
        $this->address = $address;
        $this->port = $port;
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     */
    public function start()
    {
        $this->run($this->address, $this->port);
    }

    /**
     * Launch server.
     *
     * @param string $address
     * @param int $port
     */
    private function run(string $address, int $port)
    {
        $this->setup($address, $port);

        do {
            usleep(200);
            if ($this->worker->select() < 1) {
                continue;
            }

            $this->worker->accept()->process();
        } while ($this->serverState);

        $this->handler->info("Closing...");
    }

    /**
     * {@inheritdoc}
     */
    public function stop()
    {
        $this->serverState = false;
    }

    /**
     * {@inheritdoc}
     */
    public function restart(string $address, int $port)
    {
        $this->serverState = false;
        $this->handler->info("Restarting server...");
        $this->run($address, $port);
    }

    /**
     * Create and prepare a socket worker.
     *
     * @param string $address
     * @param int $port
     */
    private function setup(string $address, int $port): void
    {
        $mainSocket = (new MainSocketBuilder())
            ->setAddress($address)
            ->setPort($port)
            ->setReuseAddress(true)
            ->create()
            ->bind()
            ->listen()
            ->build();

        $this->worker = new SocketWorker($mainSocket, $this->handler);

        $this->serverState = true;
        $this->handler->info(
            sprintf("Started listening on: %s:%d", $address, $port)
        );
    }
}
