<?php

namespace BracySocket\Server\Validators;

/**
 * Abstraction of socket validation.
 */
interface SocketValidatorInterface
{
    /**
     * Check if the socket is of valid type
     * and has no errors after creation.
     *
     * @return bool
     */
    public function isValid(): bool;
}
