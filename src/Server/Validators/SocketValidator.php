<?php

namespace BracySocket\Server\Validators;

use BracySocket\Server\Sockets\TCPSocketInterface;

/**
 * Socket validation implementation.
 */
class SocketValidator implements SocketValidatorInterface
{
    /**
     * Socket object for validation.
     *
     * @var TCPSocketInterface
     */
    private $socket;

    /**
     * SocketValidator constructor.
     *
     * @param TCPSocketInterface $socket
     */
    public function __construct(TCPSocketInterface $socket)
    {
        $this->socket = $socket;
    }

    /**
     * {@inheritdoc}
     */
    public function isValid(): bool
    {
        $descriptor = $this->socket->getDescriptor();
        $socketType = socket_getopt($descriptor, SOL_SOCKET, SO_TYPE);
        $errorStatus = socket_getopt($descriptor, SOL_SOCKET, SO_ERROR);

        return ($socketType == SOCK_STREAM && $errorStatus == 0);
    }
}
