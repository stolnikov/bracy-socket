<?php

namespace BracySocket\Server\Exceptions;

/**
 * Exception class encapsulating errors that occur
 * during socket object lifetime.
 */
class SocketException extends \RuntimeException
{
    /**
     * SocketException constructor
     *
     * @param string $message
     * @param integer $code
     */
    public function __construct($message = '', $code = 0)
    {
        $errorCode = socket_last_error();
        $errorMessage = socket_strerror($errorCode);

        parent::__construct($errorMessage, $errorCode);
    }
}
