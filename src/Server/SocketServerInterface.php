<?php

namespace BracySocket\Server;

/**
 * Abstraction of a socket server.
 */
interface SocketServerInterface
{
    /**
     * Preconfigure and run socket server.
     */
    public function start();

    /**
     * Stop listening.
     *
     * @return void
     */
    public function stop();

    /**
     * Restart server for a given address-port pair.
     *
     * @param string $address
     * @param int $port
     */
    public function restart(string $address, int $port);
}
