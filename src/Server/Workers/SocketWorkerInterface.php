<?php

namespace BracySocket\Server\Workers;

/**
 * Connection-oriented socket server API abstraction.
 */
interface SocketWorkerInterface
{
    /**
     * Select sockets that are ready to be read from.
     * Synchronize modified $read array with $this->watchList.
     *
     * @return int
     */
    public function select(): int;

    /**
     * Read from the readable sockets, call the output handler
     * and return a response.
     *
     * @return void
     */
    public function process();

    /**
     * Check if there is a status change in the listening socket.
     * It may mean that a client is trying to connect.
     *
     * @return $this
     */
    public function accept();
}
