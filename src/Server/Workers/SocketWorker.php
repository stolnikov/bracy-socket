<?php

namespace BracySocket\Server\Workers;

use BracySocket\Server\Exceptions\SocketException;
use BracySocket\Server\Handlers\HandlerInterface;
use BracySocket\Server\Sockets\ClientSocketInterface;
use BracySocket\Server\Sockets\MainSocketInterface;
use BracySocket\Server\Sockets\TCPSocketInterface;

/**
 * Connection-oriented socket server API implementation.
 */
class SocketWorker implements SocketWorkerInterface
{
    /**
     * Listening socket wrapper
     *
     * @var MainSocketInterface
     */
    private $mainSocket;

    /**
     * Output handler
     *
     * @var HandlerInterface
     */
    private $handler;

    /**
     * Array of client socket objects
     *
     * @var array
     */
    private $clients = [];

    /**
     * Array of socket objects to watch for
     * blocking status change
     *
     * @var array
     */
    private $watchList = [];

    /**
     * SocketWorker constructor
     *
     * @param MainSocketInterface $mainSocket
     * @param HandlerInterface $handler
     */
    public function __construct(
        MainSocketInterface $mainSocket,
        HandlerInterface $handler
    ) {
        $this->mainSocket = $mainSocket;
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     */
    public function select(): int
    {
        $this->clients = array_values($this->clients);
        $this->watchList = array_merge([$this->mainSocket], $this->clients);

        $watchListMapper = function (TCPSocketInterface $s) {
            return $s->getDescriptor();
        };
        $read = array_map($watchListMapper, $this->watchList);

        $write = $except = [$this->mainSocket];
        $selected = @socket_select($read, $write, $except, $tv_sec = 0);

        $watchListUpdater = function (TCPSocketInterface $s) use ($read) {
            return in_array($s->getDescriptor(), $read);
        };
        $this->watchList = array_filter($this->watchList, $watchListUpdater);

        return $selected;
    }

    /**
     * {@inheritdoc}
     */
    public function process()
    {
        // filter active client sockets to read from
        $clientFilter = function ($s1, $s2) {
            /**
             * @var TCPSocketInterface $s1
             * @var TCPSocketInterface $s2
             */
            return $s1->getDescriptor() <=> $s2->getDescriptor();
        };
        $active = array_uintersect($this->clients, $this->watchList, $clientFilter);

        foreach ($active as $key => $client) {
            /** @var ClientSocketInterface $client */
            try {
                $buffer = trim($client->read());
                $client->write($this->handler->handle($buffer));
            } catch (SocketException $e) {
                unset($this->clients[$key]);
                $client->close();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function accept()
    {
        if (in_array($this->mainSocket, $this->watchList)) {
            $newClient = $this->mainSocket->accept();
            $this->clients[] = $newClient;

            $newClient->write($this->handler->onConnect());
        }

        return $this;
    }

    /**
     * Close all active sockets.
     */
    public function __destruct()
    {
        foreach ($this->clients as $client) {
            /** @var ClientSocketInterface $client */
            $client->close();
        }

        $this->mainSocket->close();
    }
}
