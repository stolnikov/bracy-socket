<?php

namespace BracySocket\Server\Handlers;

/**
 * Abstraction of output handler. Output handler receives data and
 * processes it alone or together with dispatcher objects.
 * It is an endpoint for communication with socket worker object.
 * Also request handler is able to output service server information.
 */
interface HandlerInterface
{
    /**
     * Event handler callback when a new client is connected.
     *
     * @return string
     */
    public function onConnect(): string;

    /**
     * Handle received data from clients.
     *
     * @param string $buffer
     *
     * @return string
     */
    public function handle(string $buffer): string;

    /**
     * Callback when the socket server has extra service information
     *
     * @param string $string
     *
     * @return void
     */
    public function info(string $string);
}
