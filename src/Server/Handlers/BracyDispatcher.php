<?php

namespace BracySocket\Server\Handlers;

use Bracy\DTO\Bracy;
use Bracy\Exceptions\EmptyContentException;
use Bracy\Validators\BracyValidatorInterface;

/**
 * Bracy dispatcher implementation.
 */
class BracyDispatcher implements DispatcherInterface
{
    /**
     * @var BracyValidatorInterface
     */
    private $validator;

    /**
     * BracyDispatcher constructor
     *
     * @param BracyValidatorInterface $validator
     */
    public function __construct(BracyValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function process(string $buffer)
    {
        try {
            return sprintf(
                "Check complete. Brackets are %sbalanced." . PHP_EOL,
                $this->validator->isValid(new Bracy($buffer)) ? '' : 'un'
            );
        } catch (EmptyContentException | \InvalidArgumentException $e) {
            return sprintf("%s" . PHP_EOL, $e->getMessage());
        }
    }
}
