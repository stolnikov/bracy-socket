<?php

namespace BracySocket\Commands;

use BracySocket\Server\Handlers\BracyDispatcher;
use BracySocket\Server\Handlers\HandlerInterface;
use BracySocket\Server\SocketServerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Abstract command class with basic functionality for running socket server.
 */
abstract class AbstractCommand extends Command implements HandlerInterface
{
    /**
     * Dedicated class to produce a textual response.
     *
     * @var BracyDispatcher
     */
    protected $dispatcher;

    /**
     * Socket server instance.
     *
     * @var  SocketServerInterface
     */
    protected $socketServer;

    /**
     * Output handler instance.
     *
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var string $defaultAddress
     */
    protected $defaultAddress = '0.0.0.0';

    /**
     * @var int $defaultPort
     */
    protected $defaultPort = 10000;

    /**
     * ServerCommand constructor
     *
     * @param BracyDispatcher $dispatcher
     *
     * @throws \BadMethodCallException
     */
    public function __construct(BracyDispatcher $dispatcher)
    {
        parent::__construct();

        $this->dispatcher = $dispatcher;

        if (!function_exists('pcntl_signal')) {
            throw new \BadMethodCallException('pcntl module was not found.');
        }

        pcntl_async_signals(true);
        $this->registerSignals();
    }

    /**
     * {@inheritdoc}
     */
    public function handle(string $buffer): string
    {
        if (strlen($buffer) > 0) {
            $this->info(sprintf("<info>%s</info>", $buffer));
        }

        return $this->dispatcher->process($buffer);
    }

    /**
     * {@inheritdoc}
     */
    public function onConnect(): string
    {
        return PHP_EOL . "Welcome to Bracy TCP/IP Server." . PHP_EOL .
            "Example query string: '()()((()))'{enter}" . PHP_EOL;
    }

    /**
     * {@inheritdoc}
     */
    public function info(string $buffer)
    {
        $this->output->writeln(
            sprintf("<info>%s</info>", $buffer)
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * Register signal handlers.
     * Example:
     * pcntl_signal(SIGHUP, [$this, 'restart']);
     */
    abstract protected function registerSignals();
}
