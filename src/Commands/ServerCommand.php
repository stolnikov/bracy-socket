<?php

namespace BracySocket\Commands;

use BracySocket\Server\SocketServer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command class for manual server start.
 */
class ServerCommand extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('server:start')
            ->setDescription('Start TCP/IP server listening on user provided port.')
            ->addArgument(
                'port',
                InputArgument::REQUIRED,
                sprintf('Port number to listen on (default : %d)', $this->defaultPort)
            )
            ->addOption(
                'address',
                'a',
                InputOption::VALUE_OPTIONAL,
                sprintf('Interface to listen on (default : %d)', $this->defaultAddress),
                $this->defaultAddress
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $port = $input->getArgument('port');
        $address = $input->getOption('address');

        $this->socketServer = (new SocketServer($address, $port, $this));
        $this->socketServer->start();
    }

    /**
     * {@inheritdoc}
     */
    protected function registerSignals()
    {
    }
}
