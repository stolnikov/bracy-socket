<?php

namespace BracySocket\Commands;

use Bracy\Exceptions\EmptyContentException;
use BracySocket\Server\Helpers\YamlPortFetcher;
use BracySocket\Server\SocketServer;
use InvalidArgumentException as ArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Command class for starting server from yaml configuration data file.
 */
class ServerYamlCommand extends AbstractCommand
{
    /**
     * File path for yaml configuration file.
     *
     * @var string
     */
    private $filePath;

    /**
     * Port to listen on.
     *
     * @var int
     */
    private $port;

    /**
     * Restart socket on port change.
     *
     * @throws EmptyContentException
     */
    public function restart()
    {
        $this->info("SIGHUP signal is captured.");

        $yamlPort = YamlPortFetcher::getPort($this->filePath);
        if ($this->port != $yamlPort) {
            $this->info(
                sprintf("Configuration change detected. " . PHP_EOL .
                    "Creating a new socket on port %s.", $yamlPort)
            );

            $this->socketServer->restart($this->defaultAddress, $yamlPort);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('server:start-yaml')
            ->setDescription('Starts TCP/IP server listening on a port ' .
                'located in a user-provided .yml file.')
            ->addArgument('filepath', InputArgument::REQUIRED, 'Unix file path');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $this->filePath = $input->getArgument('filepath');

        try {
            $this->port = YamlPortFetcher::getPort($this->filePath);
            $this->socketServer = new SocketServer(
                $this->defaultAddress,
                $this->port,
                $this
            );
            $this->socketServer->start();
        } catch (EmptyContentException | ParseException | ArgumentException $e) {
            $this->info(sprintf("%s", $e->getMessage()));
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function registerSignals()
    {
        pcntl_signal(SIGHUP, [$this, 'restart']);
    }
}
